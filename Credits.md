# Credits

# Main Dependencies
* [JSON-java](https://github.com/stleary/JSON-java) - Public Domain
* [snakeyaml](https://github.com/snakeyaml/snakeyaml) - Apache License 2.0
* [JsonSchemaValidator](https://github.com/networknt/json-schema-validator) - Apache License 2.0
* [zjsonpatch](https://github.com/flipkart-incubator/zjsonpatch) - Apache License 2.0

# Sub Dependencies
* [itu](https://github.com/ethlo/itu)
* [FasterXML](https://github.com/FasterXML/jackson-core) - Apache License 2.0
* [Commons Collections](https://commons.apache.org/proper/commons-collections) - Apache License 2.0
