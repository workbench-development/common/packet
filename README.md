# Packet
<a href="https://gitlab.com/workbench-development/common/packet/-/packages">![Latest Release](https://img.shields.io/gitlab/v/release/workbench-development/common/packet?style=for-the-badge)</a>
<a href="https://www.guilded.gg/i/pB1zqebk">![Guilded Support Server](https://img.shields.io/badge/Guilded%20Support%20Server-Join-yellow?style=for-the-badge&logo=Guilded)</a>
<a href="https://www.guilded.gg/i/pB1zqebk">![Version](https://img.shields.io/badge/Java-8-red?style=for-the-badge&logo=Java)</a>

Packet is a Key Value, Data Object Class
