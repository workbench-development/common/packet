package workbench.common.packet.compiler

import workbench.common.packet.Packet
import workbench.common.packet.PacketCompiler

class JsonCompiler : PacketCompiler {
    override fun load(p: Packet, data: String): Packet {
        return Packet(data)
    }

    override fun export(p: Packet): String {
        return p.data.toString()
    }
}
