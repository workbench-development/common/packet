package workbench.common.packet.compiler

import workbench.common.packet.Packet
import workbench.common.packet.PacketCompiler
import workbench.common.packet.exception.InvalidDataException
import java.io.ByteArrayInputStream
import java.io.IOException
import java.util.*
import java.util.stream.Collectors

class PropertiesCompiler : PacketCompiler {
    override fun load(p: Packet, data: String): Packet {
        val properties = Properties()
        try {
            properties.load(ByteArrayInputStream(data.toByteArray()))
        } catch (ex: IOException) {
            throw InvalidDataException(ex.message ?: "")
        }
        properties.forEach { k: Any, v: Any -> p.put(k as String, v) }
        return p
    }

    override fun export(p: Packet): String {
        return p.getEntry<Any>().entries.stream()
            .filter { x: Map.Entry<String?, Any> -> x.javaClass.isPrimitive || classes.contains(x.javaClass) }
            .map { x: Map.Entry<String, Any> -> x.key + "=" + x.value }
            .collect(Collectors.joining())
    }

    companion object {
        private val classes: List<Class<*>> = listOf<Class<*>>(
            String::class.java,
            Int::class.java,
            Long::class.java,
            Float::class.java,
            Double::class.java,
            Boolean::class.java
        )
    }
}
