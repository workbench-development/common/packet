package workbench.common.packet.compiler

import workbench.common.packet.Packet
import workbench.common.packet.PacketCompiler

class TomlCompiler : PacketCompiler {
    override fun load(p: Packet, data: String): Packet {
        return Packet()
    }

    override fun export(p: Packet): String {
        return ""
    }
}
