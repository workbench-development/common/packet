package workbench.common.packet.compiler

import org.json.JSONObject
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.Yaml
import workbench.common.packet.Packet
import workbench.common.packet.PacketCompiler

class YamlCompiler : PacketCompiler {
    override fun load(p: Packet, data: String): Packet {
        return Packet(JSONObject(yaml.load<Map<String?, Any?>>(data)))
    }

    override fun export(p: Packet): String {
        val options = DumperOptions()
        options.defaultFlowStyle = DumperOptions.FlowStyle.BLOCK
        val yaml = Yaml(options)
        val map = yaml.load<HashMap<String, Any>>(p.export(JsonCompiler()))
        return yaml.dump(map)
    }

    companion object {
        private val yaml = Yaml()
    }
}
