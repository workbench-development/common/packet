package workbench.common.packet.compiler

import org.json.XML
import workbench.common.packet.Packet
import workbench.common.packet.PacketCompiler

class XmlCompiler : PacketCompiler {
    override fun load(p: Packet, data: String): Packet {
        return Packet(XML.toJSONObject(data))
    }

    override fun export(p: Packet): String {
        return XML.toString(p.data)
    }
}
