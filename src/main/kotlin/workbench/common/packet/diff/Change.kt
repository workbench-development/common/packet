package workbench.common.packet.diff

import workbench.common.packet.Packet

abstract class Change protected constructor(val type: ChangeType, val asPacket: Packet) {
    val path: String = asPacket.getString("path").substring(1).replace("/", ".")
}
