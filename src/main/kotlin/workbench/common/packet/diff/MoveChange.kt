package workbench.common.packet.diff

import workbench.common.packet.Packet

class MoveChange(p: Packet) : Change(ChangeType.move, p) {
    val from = p.getString("from").substring(1).replace("/", ".")

    override fun toString(): String {
        return "> \"$from\" > \"$path\""
    }
}
