package workbench.common.packet.diff

enum class ChangeType(val character: String) {
    add("+"),
    remove("-"),
    copy("&"),
    move(">"),
    replace("*"),
    list("*")
}
