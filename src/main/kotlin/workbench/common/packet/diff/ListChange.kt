package workbench.common.packet.diff

import workbench.common.packet.Packet
import java.util.stream.Collectors

class ListChange protected constructor(p: Packet?, val before: List<Any>, val after: List<Any>) : Change(ChangeType.list, p!!) {
    private val changes: MutableList<Entry> = ArrayList()

    protected fun add(slot: Int, change: Change) {
        changes.add(Entry(slot, change))
    }

    fun getChanges(): List<Entry> {
        return changes
    }

    override fun toString(): String {
        return "* \"$path\": " + changes.stream()
            .map { x: Entry -> x.getType().character + " " + x.slot + ": " + x.toString().substring(2) }
            .collect(Collectors.toList())
    }

    class Entry(slot: Int, val change: Change) {
        val slot: Long = slot.toLong()

        fun getType(): ChangeType {
            return change.type
        }

        fun getPath(): String {
            return change.path
        }

        fun getAsPacket(): Packet {
            return change.asPacket
        }
    }
}
