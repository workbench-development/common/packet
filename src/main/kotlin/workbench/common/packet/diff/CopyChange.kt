package workbench.common.packet.diff

import workbench.common.packet.Packet

class CopyChange(p: Packet) : Change(ChangeType.copy, p) {
    val from = p.getString("from").substring(1).replace("/", ".")

    override fun toString(): String {
        return "# \"$from\" > \"$path\""
    }
}
