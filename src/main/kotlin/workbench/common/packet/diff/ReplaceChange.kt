package workbench.common.packet.diff

import workbench.common.packet.Packet

class ReplaceChange(p: Packet) : Change(ChangeType.replace, p) {
    val value: Any? = p.get("value")
    val before: Any? = p.get("before")

    override fun toString(): String {
        return "* \"$path\": "
    }
}
