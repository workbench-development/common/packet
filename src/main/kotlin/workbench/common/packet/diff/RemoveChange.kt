package workbench.common.packet.diff

import workbench.common.packet.Packet

class RemoveChange(p: Packet) : Change(ChangeType.remove, p) {
    val value: Any? = p.get("value")

    override fun toString(): String {
        return "- \"$path\": $value"
    }
}
