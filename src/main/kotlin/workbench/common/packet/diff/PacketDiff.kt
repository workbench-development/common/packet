package workbench.common.packet.diff

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.flipkart.zjsonpatch.DiffFlags
import com.flipkart.zjsonpatch.JsonDiff
import workbench.common.packet.Compiler
import workbench.common.packet.Packet
import java.util.*
import java.util.stream.Collectors

class PacketDiff private constructor(target: Packet, source: Packet) {
    val add: Int
    val remove: Int
    val copy: Int
    val move: Int
    val replace: Int
    val changes: List<Change>
    init {
        val node = JsonDiff.asJson(
            getJsonNode(target),
            getJsonNode(source),
            EnumSet.of(DiffFlags.ADD_ORIGINAL_VALUE_ON_REPLACE)
        )
        var add = 0
        var remove = 0
        var copy = 0
        var move = 0
        var replace = 0
        val changes = mutableListOf<Change>()
        for (x in node) {
            val p = Packet(x.toString())
            when (x["op"].asText()) {
                "add" -> {
                    add++
                    changes.add(AddChange(p))
                }
                "remove" -> {
                    remove++
                    changes.add(RemoveChange(p))
                }
                "copy" -> {
                    copy++
                    changes.add(CopyChange(p))
                }
                "move" -> {
                    move++
                    changes.add(MoveChange(p))
                }
                "replace" -> {
                    replace++
                    changes.add(ReplaceChange(p))
                }
            }
        }
        this.add = add
        this.remove = remove
        this.copy = copy
        this.move = move
        this.replace = replace
        this.changes = changes
    }

    fun export(): List<Packet> {
        return changes.stream().map(Change::asPacket).collect(Collectors.toList())
    }

    val size: Long
        get() = changes.size.toLong()

    val isEmpty: Boolean
        get() = changes.isEmpty()

    fun patchWith(p: Packet): Packet {
        return p.patchWith(export())
    }

    override fun toString(): String {
        return changes.size.toString() + " Changes"
    }

    companion object {
        fun diff(target: Packet, source: Packet): PacketDiff {
            return PacketDiff(target, source)
        }

        private fun getJsonNode(p: Packet): JsonNode {
            try {
                return ObjectMapper().readTree(p.export(Compiler.Json))
            } catch (e: JsonProcessingException) {
                throw RuntimeException(e)
            }
        }
    }
}
