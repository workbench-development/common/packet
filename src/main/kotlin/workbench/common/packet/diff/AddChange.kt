package workbench.common.packet.diff

import workbench.common.packet.Packet

class AddChange(p: Packet) : Change(ChangeType.add, p) {
    val value: Any? = p.get("value")

    override fun toString(): String {
        return "+ \"$path\": $value"
    }
}
