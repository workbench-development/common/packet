package workbench.common.packet

enum class FailSolution {
    Empty,
    Null,
    Exception
}
