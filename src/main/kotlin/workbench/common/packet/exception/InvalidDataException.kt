package workbench.common.packet.exception

class InvalidDataException(message: String) : Exception(message)
