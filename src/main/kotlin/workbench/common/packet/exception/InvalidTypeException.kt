package workbench.common.packet.exception

class InvalidTypeException(message: String) : Exception(message)
