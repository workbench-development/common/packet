package workbench.common.packet

import workbench.common.packet.compiler.JsonCompiler
import workbench.common.packet.compiler.PropertiesCompiler
import workbench.common.packet.compiler.XmlCompiler
import workbench.common.packet.compiler.YamlCompiler

enum class Compiler(val compiler: PacketCompiler) {
    Json(JsonCompiler()),
    Yaml(YamlCompiler()),
    Xml(XmlCompiler()),
    Properties(PropertiesCompiler());

    companion object {
        @JvmStatic
        fun get(type: String): Compiler? {
            return when (type.lowercase()) {
                "json" -> Json
                "yml", "yaml" -> Yaml
                "xml" -> Xml
                "properties" -> Properties
                else -> null
            }
        }
    }
}
