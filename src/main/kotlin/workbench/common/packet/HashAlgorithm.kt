package workbench.common.packet

enum class HashAlgorithm(val algorithm: String) {
    /**
     * [FIPS PUB 202](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.202.pdf)
     */
    SHA3_512("SHA3-512"),

    /**
     * [FIPS PUB 202](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.202.pdf)
     */
    SHA3_384("SHA3-384"),

    /**
     * [FIPS PUB 202](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.202.pdf)
     */
    SHA3_256("SHA3-256"),

    /**
     * [FIPS PUB 202](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.202.pdf)
     */
    SHA3_224("SHA3-224"),

    /**
     * [FIPS PUB 180-4](https://csrc.nist.gov/publications/fips/fips180-4/fips-180-4.pdf)
     */
    SHA_512x256("SHA-512/256"),

    /**
     * [FIPS PUB 180-4](https://csrc.nist.gov/publications/fips/fips180-4/fips-180-4.pdf)
     */
    SHA_512x224("SHA-512/224"),

    /**
     * [FIPS PUB 180-4](https://csrc.nist.gov/publications/fips/fips180-4/fips-180-4.pdf)
     */
    SHA_512("SHA-512"),

    /**
     * [FIPS PUB 180-4](https://csrc.nist.gov/publications/fips/fips180-4/fips-180-4.pdf)
     */
    SHA_384("SHA-384"),

    /**
     * [FIPS PUB 180-4](https://csrc.nist.gov/publications/fips/fips180-4/fips-180-4.pdf)
     */
    SHA_256("SHA-256"),

    /**
     * [FIPS PUB 180-4](https://csrc.nist.gov/publications/fips/fips180-4/fips-180-4.pdf)
     */
    SHA_224("SHA-224"),

    /**
     * [FIPS PUB 180-4](https://csrc.nist.gov/publications/fips/fips180-4/fips-180-4.pdf)
     */
    SHA_1("SHA-1"),

    /**
     * [RFC 1321](https://tools.ietf.org/html/rfc1321)
     */
    MD5("MD5"),

    /**
     * [RFC 1319](https://tools.ietf.org/html/rfc1319)
     */
    MD2("MD2")
}
