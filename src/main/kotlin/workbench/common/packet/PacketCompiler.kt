package workbench.common.packet

interface PacketCompiler {
    fun load(p: Packet, data: String): Packet?
    fun export(p: Packet): String
}
