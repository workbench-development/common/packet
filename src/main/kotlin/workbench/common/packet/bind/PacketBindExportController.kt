package workbench.common.packet.bind

import workbench.common.packet.Packet

/**
 * Packet -> Class
 */
class PacketBindExportController {
    internal var automatic = true
    internal val binds: MutableMap<String, (p: Packet) -> Any?> = mutableMapOf()

    fun fields(): Set<String> {
        return binds.keys
    }

    fun clear() {
        binds.clear()
    }

    fun preventAutomaticNamedBinding(): PacketBindExportController {
        automatic = false
        return this
    }

    fun preventAutomaticNamedBinding(enable: Boolean): PacketBindExportController {
        automatic = enable
        return this
    }

    fun isAutomaticBinding(): Boolean {
        return automatic
    }

    fun bind(field: String): PacketBindExportController {
        binds[field] = {
            when (val obj = it.get(field)) {
                is Int -> { obj }
                is Long -> { obj }
                is Boolean -> { obj }
                is Collection<*> -> { obj }
                else -> { obj.toString() }
            }
        }
        return this
    }

    fun bind(field: String, from: String? = null): PacketBindExportController {
        binds[field] = { it.get(from ?: field) }
        return this
    }

    fun bind(field: String, convert: (Packet) -> Any?): PacketBindExportController {
        binds[field] = { convert.invoke(it) }
        return this
    }

    fun bind(field: String, convert: (Any?) -> Any?, from: String? = null): PacketBindExportController {
        binds[field] = { convert.invoke(it.get(from ?: field)) }
        return this
    }

    fun <T : Enum<T>> bindEnum(field: String, enum: Class<T>, from: String? = null): PacketBindExportController {
        binds[field] = { java.lang.Enum.valueOf(enum, it.getString(from ?: field)) }
        return this
    }
}
