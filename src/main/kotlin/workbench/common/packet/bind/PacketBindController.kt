package workbench.common.packet.bind

import workbench.common.packet.Packet

interface PacketBindController {

    fun export(p: Packet, m: PacketBindExportController): PacketBindExportController {
        return m
    }

    fun import() {

    }

}
