package workbench.common.packet.bind

import workbench.common.packet.Packet

/**
 * Class -> Packet
 */
class PacketBindInputController internal constructor() {
    internal var automatic = true
    internal val binds: MutableMap<String, (p: Packet, obj: Any?) -> Any> = mutableMapOf()

    fun fields(): Set<String> {
        return binds.keys
    }

    fun clear() {
        binds.clear()
    }

    fun preventAutomaticNamedBinding(): PacketBindInputController {
        automatic = false
        return this
    }

    fun preventAutomaticNamedBinding(enable: Boolean): PacketBindInputController {
        automatic = enable
        return this
    }

    fun isAutomaticBinding(): Boolean {
        return automatic
    }

    fun bind(field: String, to: String? = null): PacketBindInputController {
        binds[field] = { p, obj -> p.put(to ?: field, obj) }
        return this
    }

    fun bind(field: String, convert: (Any?) -> Any?, to: String? = null): PacketBindInputController {
        binds[field] = { p, obj -> p.put(to ?: field, convert.invoke(obj)) }
        return this
    }
}
