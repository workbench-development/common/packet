package workbench.common.packet.bind

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class PacketField(
    val field: String = "",
    val import: Boolean = true,
    val export: Boolean = true,
    val asString: Boolean = false,
    val allowNull: Boolean = true,
    val fallbackNull: Boolean = true
)
