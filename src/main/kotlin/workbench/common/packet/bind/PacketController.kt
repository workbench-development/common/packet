package workbench.common.packet.bind

import kotlin.reflect.KClass

@Target(AnnotationTarget.CLASS)
annotation class PacketController(val controller: KClass<out PacketBindController>)
