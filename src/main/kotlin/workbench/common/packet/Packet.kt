package workbench.common.packet

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.flipkart.zjsonpatch.CompatibilityFlags
import com.flipkart.zjsonpatch.JsonPatch
import com.networknt.schema.JsonSchemaFactory
import com.networknt.schema.SpecVersion.VersionFlag
import com.networknt.schema.ValidationMessage
import org.json.JSONArray
import org.json.JSONObject
import workbench.common.packet.bind.*
import workbench.common.packet.diff.PacketDiff
import workbench.common.packet.exception.InvalidTypeException
import workbench.common.packet.exception.ReadOnlyException
import java.io.File
import java.io.FileNotFoundException
import java.nio.file.Files
import java.security.MessageDigest
import java.util.*
import java.util.stream.Collectors
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible
import kotlin.reflect.jvm.javaField


@Suppress("MemberVisibilityCanBePrivate", "unused")
class Packet {
    var data = JSONObject()
    var readOnly: Boolean = false

    constructor()

    constructor(json: String) {
        data = if (json.startsWith("{")) JSONObject(json)
        else JSONObject().put("list", JSONArray(json))
    }

    constructor(jsonObject: JSONObject) {
        data = jsonObject
    }

    companion object {

        @JvmStatic
        fun string(data: String, compiler: Compiler): Packet? {
            return string(data, compiler, FailSolution.Exception)
        }

        @JvmStatic
        fun string(data: String, compiler: Compiler, solution: FailSolution): Packet? {
            val p: Packet?
            try {
                p = compiler.compiler.load(Packet(), data)
                if (p == null) throw NullPointerException()
            }catch (ex: Exception){
                return when (solution) {
                    FailSolution.Empty -> Packet()
                    FailSolution.Null -> null
                    FailSolution.Exception -> throw ex
                }
            }
            return p
        }

        @JvmStatic
        fun string(data: String, compiler: PacketCompiler): Packet? {
            return string(data, compiler, FailSolution.Exception)
        }

        @JvmStatic
        fun string(data: String, compiler: PacketCompiler, solution: FailSolution): Packet? {
            val p: Packet?
            try {
                p = compiler.load(Packet(), data)
                if (p == null) throw NullPointerException()
            } catch (ex: Exception) {
                return when (solution) {
                    FailSolution.Empty -> Packet()
                    FailSolution.Null -> null
                    FailSolution.Exception -> throw RuntimeException(ex)
                }
            }
            return p
        }

        @JvmStatic
        fun file(file: File): Packet {
            return file(
                file, FailSolution.Exception
            )!!
        }

        @JvmStatic
        fun file(file: File, solution: FailSolution): Packet? {
            return file(
                file, when (file.extension) {
                    "json" -> Compiler.Json
                    "yml", "yaml" -> Compiler.Yaml
                    "xml" -> Compiler.Xml
                    "properties" -> Compiler.Properties
                    else -> throw Exception("Unknown file format")
                }.compiler, solution
            )
        }

        @JvmStatic
        fun file(file: File, compiler: Compiler): Packet {
            return file(file, compiler, FailSolution.Exception)!!
        }

        @JvmStatic
        fun file(file: File, compiler: PacketCompiler): Packet {
            return file(file, compiler, FailSolution.Exception)!!
        }

        @JvmStatic
        fun file(file: File, compiler: Compiler, solution: FailSolution): Packet? {
            return file(file, compiler.compiler, solution)
        }

        @JvmStatic
        fun file(file: File, compiler: PacketCompiler, solution: FailSolution): Packet? {
            val data: String
            try {
                if (!file.exists()) throw FileNotFoundException()
                data = String(Files.readAllBytes(file.toPath()))
            } catch (ex: Exception) {
                return when (solution) {
                    FailSolution.Empty -> Packet()
                    FailSolution.Null -> null
                    FailSolution.Exception -> throw RuntimeException(ex)
                }
            }
            return string(data, compiler, solution)
        }

        @JvmStatic
        fun file(file: String): Packet {
            return file(file, FailSolution.Exception)!!
        }

        @JvmStatic
        fun file(file: String, solution: FailSolution): Packet? {
            return file(
                file, when (file.split(".", limit = 512).last()) {
                    "json" -> Compiler.Json
                    "yml", "yaml" -> Compiler.Yaml
                    "xml" -> Compiler.Xml
                    "properties" -> Compiler.Properties
                    else -> throw Exception("Unknown file format")
                }.compiler, solution
            )
        }

        @JvmStatic
        fun file(file: String, compiler: Compiler): Packet {
            return file(file, compiler, FailSolution.Exception)!!
        }

        @JvmStatic
        fun file(file: String, compiler: PacketCompiler): Packet {
            return file(file, compiler, FailSolution.Exception)!!
        }

        @JvmStatic
        fun file(file: String, compiler: Compiler, solution: FailSolution): Packet? {
            return file(file, compiler.compiler, solution)
        }

        @JvmStatic
        fun file(file: String, compiler: PacketCompiler, solution: FailSolution): Packet? {
            return file(File(file), compiler, solution)
        }

        @JvmStatic
        fun fromClass(c: Any): Packet {
            return Packet().getFromClass(c)
        }

        @JvmStatic
        fun fromClass(c: Any, controller: PacketBindInputController): Packet {
            return Packet().getFromClass(c, controller)
        }
    }

    fun export(compiler: Compiler): String {
        return compiler.compiler.export(this)
    }

    fun export(compiler: PacketCompiler): String {
        return compiler.export(this)
    }

    fun put(key: String, value: Any?): Packet {
        if (readOnly) throw ReadOnlyException()
        if (value is Packet) data.put(key, value.data)
        else if (value is List<*> && value.all { it is Packet }) data.put(key, JSONArray(value.map { (it as Packet).data }))
        else data.put(key, value)
        return this
    }

    fun putIfAbsent(key: String, value: Any?): Packet {
        if (readOnly) throw ReadOnlyException()
        if (!data.has(key)) put(key, if (value is Packet) value.data else value)
        return this
    }

    fun putWhen(`when`: Boolean, key: String, value: Any?): Packet {
        if (readOnly) throw ReadOnlyException()
        if (`when`) put(key, value)
        return this
    }

    fun putWhenIfAbsent(`when`: Boolean, key: String, value: Any?): Packet {
        if (readOnly) throw ReadOnlyException()
        if (`when`) putIfAbsent(key, value)
        return this
    }

    fun get(key: String): Any? {
        return if (data.has(key)) data[key] else null
    }

    inline fun <reified T> get(key: String, defaultValue: T): T {
        return (if (data.has(key) && data[key] is T) data[key] as T else defaultValue)
    }

    inline fun <reified T> getOrNull(key: String): T? {
        return (if (data.has(key) && data[key] is T) data[key] as T else null)
    }

    fun getString(key: String): String {
        return getString(key, "")
    }

    fun getString(key: String, defaultValue: String): String {
        return if (data.has(key)) if (data[key] is String) data.getString(key) else data[key].toString() else defaultValue
    }

    fun getStringOrNull(key: String): String? {
        return if (data.has(key) && data[key] != null) (if (data[key] is String) data.getString(key) else data[key].toString()) else null
    }

    fun getInt(key: String): Int {
        return getInt(key, 0)
    }

    fun getInt(key: String, defaultValue: Int): Int {
        return if (data.has(key) && data[key] is Int) data.getInt(key) else defaultValue
    }

    fun getIntOrNull(key: String): Int? {
        return if (data.has(key) && data[key] is Int) data.getInt(key) else null
    }

    fun getLong(key: String): Long {
        return getLong(key, 0)
    }

    fun getLong(key: String, defaultValue: Long): Long {
        return if (data.has(key) && (data[key] is Int || data[key] is Long)) data.getLong(key) else defaultValue
    }

    fun getLongOrNull(key: String): Long? {
        return if (data.has(key) && (data[key] is Int || data[key] is Long)) data.getLong(key) else null
    }

    fun getBoolean(key: String): Boolean {
        return getBoolean(key, false)
    }

    fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return if (data.has(key) && (data[key] is Boolean)) data.getBoolean(key) else defaultValue
    }

    fun getList(key: String): MutableList<String> {
        return getList(String::class.java, key)
    }

    fun getAnyList(key: String): MutableList<Any> {
        return getList(Any::class.java, key)
    }

    fun getAnyNullableList(key: String): MutableList<Any?> {
        return getNullableList(Any::class.java, key)
    }

    fun <T> getList(type: Class<T>, key: String): MutableList<T> {
        return getList(type, key, ArrayList())
    }

    fun <T> getList(type: Class<T>, key: String, defaultValue: MutableList<T>): MutableList<T> {
        if (!data.has(key)) return defaultValue
        var list = data[key]
        if (list is JSONArray) list = list.toMutableList()
        if (list !is Collection<*>) return defaultValue
        return list.filterIsInstance(type).toMutableList()
    }

    fun <T> getNullableList(type: Class<T>, key: String): MutableList<T?> {
        return getNullableList(type, key, mutableListOf())
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> getNullableList(type: Class<T>, key: String, defaultValue: MutableList<T?>): MutableList<T?> {
        if (!data.has(key)) return defaultValue
        var list = data[key]
        if (list is JSONArray) list = list.toMutableList()
        if (list !is Collection<*>) return defaultValue
        return list.filter { it == null || type.isInstance(it) }.toMutableList() as MutableList<T?>
    }

    fun getPacketList(key: String): MutableList<Packet> {
        if (!data.has(key)) return mutableListOf()
        var list = data[key]
        if (list is JSONArray) list = list.toMutableList()
        if (list !is Collection<*>) return mutableListOf()
        return list.map { if (it is JSONObject) Packet(it) else it }.filterIsInstance<Packet>().toMutableList()
    }

    fun addListItem(key: String, vararg data: Any?): Packet {
        return addListItem(key, listOf(*data))
    }

    fun addListItem(key: String, index: Int, vararg `object`: Any?): Packet {
        return addListItem(key, index, listOf(*`object`))
    }

    fun addListItem(key: String, `object`: Collection<Any?>): Packet {
        return addListItem(key, 0, `object`)
    }

    @Suppress("UNCHECKED_CAST")
    fun addListItem(key: String, index: Int, collection: Collection<Any?>): Packet {
        if (readOnly) throw ReadOnlyException()
        if (data.has(key)) { if (data.get(key) !is Collection<*> && data.get(key) !is JSONArray) throw InvalidTypeException("'$key' is not a Collection") }
        else data.put(key, emptyList<Any?>())
        val list = getList(Any::class.java, key) as MutableList<Any?>
        list.addAll(index, collection)
        put(key, list)
        return this
    }

    fun modifyList(key: String, modify: (MutableList<String>) -> List<String>): Packet {
        if (readOnly) throw ReadOnlyException()
        if (data.has(key)) if (data.get(key) !is Collection<*> && data.get(key) !is JSONArray) throw InvalidTypeException("'$key' is not a Collection")
        else data.put(key, emptyList<Any?>())
        val list = modify.invoke(getList(String::class.java, key))
        put(key, list)
        return this
    }

    fun <T> modifyList(type: Class<T>, key: String, modify: (MutableList<T>) -> List<T>): Packet {
        if (readOnly) throw ReadOnlyException()
        if (data.has(key)) if (data.get(key) !is Collection<*> && data.get(key) !is JSONArray) throw InvalidTypeException("'$key' is not a Collection")
        else data.put(key, emptyList<Any?>())
        put(key, modify.invoke(getList(type, key)))
        return this
    }

    @Suppress("UNCHECKED_CAST")
    fun modifyObjektList(key: String, modify: (MutableList<Any?>) -> List<Any?>): Packet {
        if (readOnly) throw ReadOnlyException()
        if (data.has(key)) if (data.get(key) !is Collection<*> && data.get(key) !is JSONArray) throw InvalidTypeException("'$key' is not a Collection")
        else data.put(key, emptyList<Any?>())
        put(key, modify.invoke(getList(Any::class.java, key) as MutableList<Any?>))
        return this
    }

    fun removeListItem(key: String, vararg elements: Any?): Packet {
        return removeListItem(key, listOf(*elements))
    }

    @Suppress("UNCHECKED_CAST")
    fun removeListItem(key: String, objects: Collection<Any?>): Packet {
        if (readOnly) throw ReadOnlyException()
        if (data.has(key)) if (data.get(key) !is Collection<*> && data.get(key) !is JSONArray) throw InvalidTypeException("'$key' is not a Collection")
        else data.put(key, emptyList<Any?>())
        val list = getList(Any::class.java, key) as MutableList<Any?>
        list.removeAll(objects)
        put(key, list)
        return this
    }

    @Suppress("UNCHECKED_CAST")
    fun removeListItem(key: String, index: Int): Packet {
        if (readOnly) throw ReadOnlyException()
        if (data.has(key)) if (data.get(key) !is Collection<*> && data.get(key) !is JSONArray) throw InvalidTypeException("'$key' is not a Collection")
        else data.put(key, emptyList<Any?>())
        val list = getList(Any::class.java, key) as MutableList<Any?>
        list.removeAt(index)
        put(key, list)
        return this
    }

    inline fun <reified T : Enum<T>> getEnum(key: String, defaultValue: T): T {
        if (!data.has(key)) return defaultValue
        val enum = get(key)!!
        return if (enum is T) enum else enumValueOf<T>(enum.toString())
    }

    inline fun <reified T : Enum<T>> getEnumOrNull(key: String): T? {
        if (!data.has(key)) return null
        val enum = data.get(key)
        return if (enum is T) enum else enumValueOf<T>(enum.toString())
    }

    fun modifyWhen(`when`: Boolean, c: (Packet) -> Unit): Packet {
        if (readOnly) throw ReadOnlyException()
        if (`when`) c.invoke(this)
        return this
    }

    fun has(key: String, vararg additional: String): Boolean {
        if (!data.has(key)) return false
        for (a in additional) if (!data.has(a)) return false
        return true
    }

    fun has(keys: Collection<String>): Boolean {
        for (k in keys) if (!data.has(k)) return false
        return true
    }

    fun hasOnce(vararg key: String): Boolean {
        for (p in key) if (data.has(p)) return true
        return false
    }

    fun hasOnce(key: Collection<String>): Boolean {
        for (p in key) if (data.has(p)) return true
        return false
    }

    fun deepHas(vararg part: Collection<String>): Boolean {
        for (s in part) for (x in s) if (!deepHas(x)) return false
        return true
    }

    fun deepHas(vararg part: String): Boolean {
        var p = data
        for (i in part.indices) {
            if (i == part.size - 1) return p.has(part[i])
            else {
                if (!p.has(part[i])) return false
                p = p.getJSONObject(part[i])
            }
        }
        return false
    }

    fun remove(vararg keys: String): Packet {
        if (readOnly) throw ReadOnlyException()
        for (k in keys) data.remove(k)
        return this
    }

    fun isNull(key: String): Boolean {
        return !has(key) && get(key) == null
    }

    fun clear(): Packet {
        if (readOnly) throw ReadOnlyException()
        data.clear()
        return this
    }

    fun size(): Int {
        return data.length()
    }

    fun isEmpty(): Boolean {
        return data.isEmpty
    }

    fun keys(): Set<String> {
        return data.keySet()
    }

    fun getPacket(key: String, vararg sub: String): Packet? {
        var packet = data
        if (!packet.has(key)) return null
        packet = packet.getJSONObject(key)
        for (k in sub) {
            if (!packet.has(k)) return null
            packet = packet.getJSONObject(k)
        }
        return Packet(packet)
    }

    fun getEntryPackets(): HashMap<String, Packet> {
        if (data.isEmpty) return HashMap()
        val map = HashMap<String, Packet>()
        val packet: Packet = copy()
        data.keySet().stream().filter { x: String? -> data[x] is JSONObject }
            .forEachOrdered { x: String -> map[x] = Packet(packet.data.getJSONObject(x)) }
        return map
    }

    /**
     * Get all Entry's in the current folder
     * @param <T> Result Value Type
    </T> */
    inline fun <reified T> getEntry(): HashMap<String, T> {
        if (data.isEmpty) return HashMap()
        val map = HashMap<String, T>()
        val packet: Packet = copy()
        data.keySet().stream().filter { x: String? -> data[x] is T }
            .forEachOrdered { x: String -> map[x] = packet.get(x) as T }
        return map
    }

    fun getEntryPacketsArray(): MutableList<Packet> {
        if (data.isEmpty) return ArrayList()
        val packet: Packet = copy()
        return data.keySet().stream().filter { x: String? -> data[x] is JSONObject }
            .map { x: String? -> Packet(packet.data.getJSONObject(x))
        }.collect(Collectors.toList())
    }

    fun getType(key: String): Class<*>? {
        if (data.has(key)) return data[key].javaClass
        return null
    }

    fun generateKeyMap(): HashMap<String, Any?> {
        @Suppress("UNCHECKED_CAST")
        fun generateKeyMapEntry(part: String, obj: Map<String, Any?>): HashMap<String, Any?> {
            val data = HashMap<String, Any?>()
            for ((key, value) in obj) {
                val str = if (part.isNotEmpty()) "$part.$key" else key
                if (value is Map<*, *>) data.putAll(generateKeyMapEntry(str, value as HashMap<String, Any?>))
                else data[str] = value
            }
            return data
        }

        return generateKeyMapEntry("", data.toMap())
    }

    inline fun <reified T> generateKeyMap(): Map<String, T> {
        return HashMap(generateKeyMap().filter { it.value is T }.map { Pair(it.key, it.value as T) }.toMap())
    }

    /**
     * Json Schema Validation
     */
    fun check(schema: File): Set<ValidationMessage> {
        return check(file(schema, Compiler.Json, FailSolution.Empty)!!, VersionFlag.V7)
    }

    /**
     * Json Schema Validation
     */
    fun check(schema: File, version: VersionFlag): Set<ValidationMessage> {
        return check(file(schema, Compiler.Json, FailSolution.Empty)!!, version)
    }

    /**
     * Json Schema Validation
     */
    fun check(schema: Packet): Set<ValidationMessage> {
        return check(schema, VersionFlag.V7)
    }

    /**
     * Json Schema Validation
     */
    fun check(schema: Packet, version: VersionFlag): Set<ValidationMessage> {
        return JsonSchemaFactory.getInstance(version).getSchema(getJsonNode(schema)).validate(getJsonNode(this))
    }

    /**
     * Target -> Source
     */
    fun diff(source: Packet): PacketDiff {
        return PacketDiff.diff(this, source)
    }

    fun patchWith(diff: List<Packet>): Packet {
        return patchWith(diff, EnumSet.noneOf(CompatibilityFlags::class.java))
    }

    fun patchWith(diff: List<Packet>, flags: EnumSet<CompatibilityFlags>): Packet {
        val changes = ObjectMapper().createArrayNode().addAll(diff.stream().map { p: Packet -> this.getJsonNode(p) }.collect(Collectors.toList()))
        val node = getJsonNode(this)
        JsonPatch.validate(changes)
        data = JSONObject(JsonPatch.apply(changes, node, flags).toString())
        return this
    }

    fun mergeFrom(source: Packet): Packet {
        PacketDiff.diff(this, source).patchWith(this)
        return this
    }

    fun mergeFrom(source: List<Packet>): Packet {
        for (p in source) PacketDiff.diff(this, p).patchWith(this)
        return this
    }

    private fun getJsonNode(p: Packet): JsonNode {
        try {
            return ObjectMapper().readTree(p.export(Compiler.Json))
        } catch (e: JsonProcessingException) {
            throw RuntimeException(e)
        }
    }

    fun <T : Any> applyToClass(obj: T): T{
        applyToClass(obj, PacketBindExportController())
        return obj
    }

    fun <T : Any> applyToClass(obj: T, controller: PacketBindController): T {
        applyToClass(obj, controller.export(Packet(), PacketBindExportController()))
        return obj
    }

    fun <T : Any> applyToClass(obj: T, controller: PacketBindExportController): T {
        obj::class.memberProperties
            .filter { it.javaField?.isAnnotationPresent(PacketField::class.java) ?: false }
            .filter { it is KMutableProperty<*> }
            .map {
                val annotation = it.javaField!!.getAnnotation(PacketField::class.java)
                Pair(annotation.field.ifEmpty { it.name }, it as KMutableProperty<*>)
            }
            .filter {
                if (controller.binds.containsKey(it.first)) return@filter true
                if (controller.automatic) {
                    //val pc = it.second.javaField!!.type.getAnnotation(PacketController::class.java)
                    controller.bind(it.first)
                    return@filter true
                }
                return@filter false
            }
            .forEach {
                try {
                    val x = controller.binds[it.first]!!.invoke(this)
                    it.second.isAccessible = true
                    it.second.setter.call(obj, x)
                    it.second.isAccessible = false
                }catch (_: Exception) { }
            }
        return obj
    }

    fun getFromClass(c: Any): Packet {
        return getFromClass(c, PacketBindInputController())
    }

    fun getFromClass(obj: Any, controller: PacketBindInputController): Packet {
        if (readOnly) throw ReadOnlyException()
        obj::class.memberProperties
            .filter { it.javaField?.isAnnotationPresent(PacketField::class.java) ?: false }
            .map {
                val annotation = it.javaField!!.getAnnotation(PacketField::class.java)
                Triple(annotation.field.ifEmpty { it.name }, annotation, it as KProperty<*>)
            }
            .filter {
                if (controller.binds.containsKey(it.first)) return@filter true
                if (controller.automatic) {
                    controller.bind(it.first)
                    return@filter true
                }
                return@filter false
            }
            .forEach {
                try {
                    val o = it.third.getter.call(obj)
                    if (o == null) { if (it.second.allowNull) put(it.first, null) } else {
                        if (it.second.asString) put(it.first, o.toString())
                        else {
                            when (o) {
                                is Int, is Long, is String, is Boolean, is Float, is Double -> put(it.first, o)
                                is Collection<*> -> put(it.first, o)
                                else -> put(it.first, fromClass(o))
                            }
                        }
                    }
                }catch (_: Exception) {
                    if (it.second.fallbackNull) put(it.first, null)
                }
            }
        return this
    }

    fun hash(): String {
        return hash(HashAlgorithm.SHA3_512)
    }

    fun hash(algorithm: HashAlgorithm): String {
        val data = generateKeyMap().entries.stream()
            .filter { x: Map.Entry<String, Any?> ->
                !x.key.split(".").dropLastWhile { it.isEmpty() }
                    .toTypedArray()[x.key.split(".").dropLastWhile { it.isEmpty() }
                    .toTypedArray().size - 1]
                    .startsWith("_")
            }
            .sorted { a: Map.Entry<String, Any?>, b: Map.Entry<String, Any?> -> a.key.compareTo(b.key) }.collect(Collectors.toList()).toString()
        val md = MessageDigest.getInstance(algorithm.algorithm)
        return md.digest(data.toByteArray()).fold("") { str, it -> str + "%02x".format(it) }
    }

    fun readonly(): Packet {
        readOnly = true
        return this
    }

    fun isReadonly(): Boolean {
        return readOnly
    }

    fun getJSONObject(): JSONObject {
        return JSONObject(data, *data.keySet().toTypedArray<String>())
    }

    /**
     * Returns packet as json, with line separation
     */
    fun print(): String {
        return data.toString(1)
    }

    override fun toString(): String {
        return "Packet$data"
    }

    fun copy(): Packet {
        return Packet(getJSONObject())
    }
}
